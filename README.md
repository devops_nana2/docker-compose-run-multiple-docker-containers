## dockerize-nodejs-application-and-push-to-private-docker-registry-ecr-aws

## installing and configuring AWS CLI

    $brew install awsclie
    
    $aws configure (need to provide access key, secret key, display format json)

## Retrieve an authentication token and authenticate your Docker client to your registry ECR.
    Use the AWS CLI

    $ aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 910933064963.dkr.ecr.us-east-1.amazonaws.com
    Login Succeeded

#### To build a docker image from the application

    $docker build -t my-app:1.8 .       

    The dot "." at the end of the command denotes location of the Dockerfile.**

    $docker images
        
#### **After the build completes, tag your image so you can push the image to this repository:**
    
    $docker tag my-app:1.8 910933064963.dkr.ecr.us-east-1.amazonaws.com/my-app:1.8

#### Run the following command to push this image to your newly created AWS repository:

    $docker push 910933064963.dkr.ecr.us-east-1.amazonaws.com/my-app:1.8

We can view docker image my-app -> Amazon ECR Private registry

Now if we want to upload another version need to follow the same steps
    
    $docker build -t my-app:1.8 .
    $docker tag my-app:1.1 910933064963.dkr.ecr.us-east-1.amazonaws.com/my-app:1.1
    $docker push 910933064963.dkr.ecr.us-east-1.amazonaws.com/my-app:1.1

    REPOSITORY                                            TAG       IMAGE ID       CREATED             SIZE
910933064963.dkr.ecr.us-east-1.amazonaws.com/my-app   1.1       b103b69f1a16   About an hour ago   163MB
my-app                                                1.1       b103b69f1a16   About an hour ago   163MB
910933064963.dkr.ecr.us-east-1.amazonaws.com/my-app   1.8       fc62878c62eb   4 days ago          163MB
